import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key, required this.myList});
  final List<String> myList;

  @override
  State<StatefulWidget> createState() {
    return _MainScreen();
  }
}

class _MainScreen extends State<MainScreen> {
  bool icon = false;

  void changeIcon() {
    setState(() {
      icon = !icon;
    });
  }

  @override
  Widget build(BuildContext context) {
    var iconFavorite = Icons.star;

    if (icon) {
      iconFavorite = Icons.star_border;
      print("if");
    }

    return Scaffold(
      appBar: AppBar(
        actions: [IconButton(onPressed: changeIcon, icon: Icon(iconFavorite))],
        title: const Text('App bar'),
      ),
      body: Container(
        height: 200,
        margin: const EdgeInsets.all(30),
        // padding: const EdgeInsets.all(30),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.blue[200],
        ),
        child: Center(
          child: Column(
            children: [
              const Text(
                'Body',
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontSize: 24,
                ),
              ),
              for (String item in widget.myList)
                TextButton(
                  onPressed: () {},
                  child: Text(item.toString()),
                ),
              const SizedBox(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}
